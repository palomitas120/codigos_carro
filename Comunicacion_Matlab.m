%Comunicacion bluetooth entre ESP32 y MATLAB

b=Bluetooth("ESP32",1) % Objeto tipo Bluetooth

AngZ_MPU = [];  %Vector de angulo en Z obtenido con MPU
AngZ_ENC = [];  %Vector de angulo en Z obtenido con Encoders
AngS = [];      %Vector de angulo con servomotor
distancia = []; %Vector de distancia captado con sensor
X_MPU = [];     %Vector posicion en x captado con MPU
Y_MPU = [];     %Vector posicion en y captado con MPU
X_ENC = [];     %Vector posicion en x captado con Encoders    
Y_ENC = [];     %Vector posicion en y captado con Encoders

tiempo = [];    %Vector tiempo          

valorAnt = 0;   %Variable guardar valor anterior de tiempo

fopen(b)    %Se abre puerto de comunicacion serial
fwrite(b,"160;160","char"); %Se envia velocidades en Rad/s que se quiere "MD;MI"

tic

for i=1:70    %Se establece un numero de muestras deseado    
    tic
    % Se obtienen los datos enviados desde el ESP32
    sensor=scanstr(b,';');
    distancia(i) = cell2mat(sensor(1));
    AngS(i) = cell2mat(sensor(2));
    AngZ_MPU(i) = cell2mat(sensor(3));
    AngZ_ENC(i) = cell2mat(sensor(4));
    X_MPU(i) = cell2mat(sensor(5));
    Y_MPU(i) = cell2mat(sensor(6));
    X_ENC(i) = cell2mat(sensor(7));
    Y_ENC(i) = cell2mat(sensor(8));
    
    
    valorA = toc;
    tiempo(i)=valorA+valorAnt; %Se calcula tiempo entre muestras
    valorAnt = tiempo(i) 
   
    
end
fwrite(b,"0;0","char");   %(OPCIONAL)Finalmente se puede detener el carro 
fclose(b)                 %Se cierra puerto MATLAB
     
%Se grafican los datos obtenidos      
     
     figure(1);
     plot(tiempo,X_MPU,'-k'),hold on
     plot(tiempo,Y_MPU,'-b')
     xlabel("Tiempo(s)")
     ylabel("Desplazamiento(m)")
     title("Desplazamiento X, Y vs tiempo con MPU")
   
     
     figure(2);
     plot(tiempo,X_ENC,'-k'),hold on
     plot(tiempo,Y_ENC,'-b')
     xlabel("Tiempo(s)")
     ylabel("Desplazamiento(m)")
     title("Desplazamiento X, Y vs tiempo con Encoders")
  
     figure(3);
     plot(tiempo,AngZ_MPU,'-k'),hold on
     plot(tiempo,AngZ_ENC,'-b')
     xlabel("Tiempo(s)")
     ylabel("Direccion(°)")
     title("Direccion Angulo Z MPU y Encoders")
     
     figure(4);
     plot(AngS,distancia,'.k')
     xlabel("Angulo servomotor")
     ylabel("Distancia(m)")
     title("Distancia en funcion del angulo del servomotor")
     
     
     


