//Actualizado al 2 de Octubre 2021
#include <analogWrite.h>
#include "Wire.h" // librería Wire.h
#include "BluetoothSerial.h"
#include <Separador.h>


#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#include <math.h>
BluetoothSerial SerialBT;
Separador s;

#include <VL53L0X.h>
VL53L0X sensor;

#include <Servo_ESP32.h>
Servo_ESP32 servo;

       
//----------------------------------------------------------------------------Variables Encoder------------------------------------------------------------------------------------//
          int PinA_MI = 25; 
          int PinB_MI = 33; 
          int PinA_MD = 32; 
          int PinB_MD = 35; 
          volatile signed int contador_MI;
          volatile signed int contador_MD;
          volatile int estado_MI, estadoAnterior_MI, estado_MD, estadoAnterior_MD;
          volatile int dir_MD , dir_MI , vuelta_MD, vuelta_MI;
          float tiempo_actual = 0;
          float tiempo_total = 0;
          float Wp_MD= 0;
          float Wp_MI = 0;
          
          long tiempo_prevENCS=0;         // para sacar dtENCS para encoders
          float dtENCS=0;                 // dtENCS con el que la funcion de encoders saca la integral
          float vpENCS = 0;               //Velocidad lineal encoder
          float wpENCS = 0;               //Velocidad angular encoder
          float dxENCS=0;                 //velocidad en x de los encoders
          float dyENCS=0;                 //velocidad en y de los encoders
          volatile float xENCS=0;         //pocisión en x de los encoders
          volatile float yENCS=0;         //pocisión en y de los encoders
          float tethaENCS=0;              //posición angular de los encoders

          String SxENCS;                  //el string para ver X en el MR inercial respecto a los encoders
          String SyENCS;                  //el string para ver Y en el MR inercial respecto a los encoders
          String StethaENCS;              //el string para ver tetha en el MR inercial respecto a los encoders

          float radio=0.0105;            //radio de las llantas
          float longitud=0.1;            //longitud de centro de la llanta izquierda a la derecha

          

//----------------------------------------------------------------------------Variables Servomotor------------------------------------------------------------------------------------//
          const int PinS=23;             // pin del servo
          float pausa;                   // pausa que debe hacer el servo
          int as=0;                    // valor actual de grados del servo
          String Sangs;                  //  String del valor de grados as (temporal)
        
//-------------------------------------------------------------------------------Variables Motores------------------------------------------------------------------------------------///
          int PinMI1 = 2;                 // pin 1 motor izquierdo
          int PinMI2 = 4;                 // pin 2 motor izquierdo 
          int PinMIPWM = 15;              // PWM motor izquierdo
          int PinMD1 = 16;                // pin 1 motor derecho 
          int PinMD2 = 17;                // pin 2 motor derecho
          int PinMDPWM = 5;               // PWM motor derecho
          String valor;                   // el char para indicaar hacia donde se mueve el carro
          int PWM_MD = 0;                 //Valor de PWM para motor derecho
          int PWM_MI = 0;                 //Valor de PWM para motor izquierdo

          int channelMD=9;
          int channelMI=8;

          const int freq=100;
          const int resolution=10;

          float e_MD = 0;              //error motor derecho
          float u_MD = 0;              //salida motor derecho
          float emd_MD = 0;            //error dos veces anterior motor derecho
          float emu_MD = 0;            //error una anterior motor derecho
          float umu_MD = 0;            //salida una anterior motor derecho

          float e_MI = 0;              //error motor izquierdo
          float u_MI = 0;              //salida motor izquierdo
          float emd_MI = 0;            //error dos veces anterior motor izquierdo
          float emu_MI = 0;            //error una anterior motor izquierdo
          float umu_MI = 0;            //salida una anterior motor izquierdo

          float refe_MD = 0;
          float refe_MI = 0;

          const float T = 0.025; //En seg  //kd=0.0109153 , kp = 0.505906 , ki = 1.658080
          float Ki_MD = 1.658080;
          float Kd_MD = 0.0109153;
          float Kp_MD = 0.505906;
          
          float b0_MD = (Kp_MD*T + Ki_MD*pow(T,2)+ Kd_MD)/T;
          float b1_MD = -(Kp_MD*T + 2*Kd_MD)/T;
          float b2_MD = Kd_MD/T;

          float Ki_MI = 1.4;         //kd=0.0147313 , kp =  0.62092 , ki = 2.1209
          float Kd_MI = 0.0147313;
          float Kp_MI = 0.62092;
          
          float b0_MI = (Kp_MI*T + Ki_MI*pow(T,2)+ Kd_MI)/T;
          float b1_MI = -(Kp_MI*T + 2*Kd_MI)/T;
          float b2_MI = Kd_MI/T;
          
//------------------------------------------------------------------------------Variables de la MPU-----------------------------------------------------------------------------------//
          
          const int MPU = 0x68;                //Direccion I2C de la IMU
           
          
          #define A_R 16384.0 // 32768/2  //Ratios de conversion
          #define G_R 131.0 // 32768/250  //Ratios de conversion
           
          #define RAD_A_DEG = 57.295779         //Conversion de radianes a grados 180/PI
           
          int16_t AcX, AcY, AcZ, GyX, GyY, GyZ; // valores de 16 bits que da la MPU
           
          //Angulos
          float Acc[2];                   //array aceleraciones
          float Gy[3];                    // array velocimetro
          float Angle[3];                 // array para angulos
          
          
          long tiempo_prev=0;             // para sacar dt1 para MPU
          float dt=0;                     // dt con el que la MPU saca la integral

          float vp=0;                     // valor de Vp
          String Svp;                     // el string para ver Vp (temporal)

          float wp;                       // valor de Wp
          String Swp;                     // el string para ver Wp (temporal)

          float angz;                     // valor de angz
          String Sangz;                   // el string para ver angZ (temporal)

          float dxMPU = 0;                // valor de velocidad X en el MR del carrito
          String SdxMPU;                  // el string para ver dx en el MR del carrito (temporal)
          
          float dyMPU = 0;                // valor de velocidad Y en el MR del carrito
          String SdyMPU;                  // el string para ver dy en el MR del carrito (temporal)
          
          float xMPU = 0;                 // valor de posición en X en el MR inercial con respeto a la MPU
          String SxMPU;                   // el string para ver X en el MR inercial con respectoa la MPU (permanente)
          
          float yMPU = 0;                 // valor de posición en Y en el MR inercial con respecto a la MPU
          String SyMPU;                   // el string para ver X en el MR inercial con respectoa la MPU (permanente)
          
          String Sdata;                   // la cadena que se envía a matlab

//-------------------------------------------------------------------------Variables sensor de distancia------------------------------------------------------------------------------//
          float distancia;                //medicion distancia
          String Sdistancia;              //String para ver distancia

//-----------------------------------------------------------------------Variables conversion int a String-----------------------------------------------------------------------------//
          char tmp_str[7];                // Variable Float to String
          char tmp_str_int[7];            // Variable Int to String
          
//--------------------------------------------------------------------------tiempos de muestreo carro---------------------------------------------------------------------------------//
          
          volatile unsigned muActual=0;   //para hallar dt
          volatile unsigned muAnterior=0; //para hallar dt
          volatile unsigned deltaMuestreo=0; //dt
          int numeros_T = 0;              //numeros enteros del tiempo de muestreo del motor 
//----------------------------------------------------------------------------pocisión deseada-------------------------------------------------------------------------------------//

          float Xactual;
          float Yactual;
          float Xdeseado = 0.9;
          float Ydeseado = 0.20;

          float TETHAactual=0;
          float TETHAdeseado=0;
     

          float TETHAe=0;               //error k
          float TETHAe_1=0;             //error k-1
          float TETHAe_2=0;             //error k-2

          float b0=0;                   //para hallar WP
          float b1=0;                   //para hallar WP
          float b2=0;                   //para hallar WP

          float Kp=5;                   //Kp para wp
          float Ki=6;                   //Ki para wp
          float Kd=2;                 //Kd para wp
          
          float PM;                     //periodo de muestreo

          double WPdeseado = 0;              //Wp después de control
          double VPdeseado = 0;              //VP después del control

          double WDdeseado = 0;
          double WIdeseado = 0;
//---------------------------------------------------Variables evacion de obstaculos -----------------------------------------------------------------
          float  obs1x,obs2x,obs3x,obs4x,obs5x;
          float  obs1y,obs2y,obs3y,obs4y,obs5y;
          float  ks1 = 0.25;  //Ponderacion segun el angulo del servo para sensor de distancia
          float  ks2 = 0.5;
          float  ks3 = 1;
          float  ks4 = 0.5;
          float  ks5 = 0.25;
          float  xto,yto,obs1,THETAto;

void IRAM_ATTR conteo_M() { //Interrupcion para leer los pulsos de los encoders y asi optener velocidad
 
        estado_MD = digitalRead(PinA_MD)<<1|digitalRead(PinB_MD);
        estado_MI = digitalRead(PinA_MI)<<1|digitalRead(PinB_MI);
        
        dir_MD = estado_MD<<2|estadoAnterior_MD;
        dir_MI = estado_MI<<2|estadoAnterior_MI;
        
        if(dir_MD == 2||dir_MD == 4||dir_MD == 13||dir_MD == 11){contador_MD--;}
        if(dir_MD == 8||dir_MD == 14||dir_MD == 7||dir_MD == 1){contador_MD++;}
        if(dir_MI == 2||dir_MI == 4||dir_MI == 13||dir_MI == 11){contador_MI++;}
        if(dir_MI == 8||dir_MI == 14||dir_MI == 7||dir_MI == 1){contador_MI--;}
        estadoAnterior_MD = estado_MD ;
        estadoAnterior_MI = estado_MI ; 
       
        //Se cancela interrupcion 
        detachInterrupt(PinA_MD);
        detachInterrupt(PinB_MD);
        detachInterrupt(PinA_MI);
        detachInterrupt(PinB_MI);
  
}

void setup(){
         //pines motores
         pinMode(PinMI1,OUTPUT);
         pinMode(PinMI2,OUTPUT);
         pinMode(PinMD1,OUTPUT);
         pinMode(PinMD2,OUTPUT);
         

         ledcSetup(channelMI, freq, resolution);
         ledcSetup(channelMD, freq, resolution);

         ledcAttachPin(PinMIPWM, channelMI);
         ledcAttachPin(PinMDPWM, channelMD);

         servo.attach(PinS);

         //Pines Encoder
         pinMode(PinA_MI,INPUT);
         pinMode(PinB_MI,INPUT);
         pinMode(PinA_MD,INPUT);
         pinMode(PinB_MD,INPUT);
         
         //inicialización de la MPU
         Wire.begin(); // D2(GPIO4)=SDA / D1(GPIO5)=SCL
         Wire.beginTransmission(MPU);
         Wire.write(0x6B);
         Wire.write(0);
         Wire.endTransmission(true);
         
         //incialización bluetooth
         Serial.begin(115200); 
         SerialBT.begin("ESP32"); //Bluetooth device name
         Serial.println("The device started, now you can pair it with bluetooth!");

        sensor.init();
        sensor.setTimeout(5);
      
        #if defined LONG_RANGE
        sensor.setSignalRateLimit(0.1);
        sensor.setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
        sensor.setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);
        #endif
      
        #if defined HIGH_SPEED
        sensor.setMeasurementTimingBudget(20000);  // minimum timing budget 20 ms
        #elif defined HIGH_ACCURACY
        sensor.setMeasurementTimingBudget(200000);
        #endif
      
        sensor.startContinuous();
        
        PWM_MD = 0;
        PWM_MI = 0;        
} 

void loop(){
        
        // rota el servo de 0 a 180
        for(int as=0;as<=180;as++){     
            funcionGeneral(as);        
            
        }
        
         //rota el servo de 180 a 0
        for(int as=180;as>=0;as--){  
            funcionGeneral(as);  
                      
        }   
} 


void funcionGeneral(int angs){

      velocidad_M();   //Se toma la velocidad de los motores
   
      controlPID_MD(refe_MD);   //Se aplica el control de velocidad al motor derecho
      controlPID_MI(refe_MI);   //Se aplica el control de velocidad al motor izquierdo
      movimiento(PWM_MD,PWM_MI); //Se aplica el PWM obtenido de los controles al puente H
      evitar(angs); 
      if(numeros_T == 4){ //Asegura que las acciones se realicen cada 4*T periodos de muestreo del motor o 100ms   
              MPU6050();
              Serial.println("Posicion X = "+String(SxENCS)); 
              Serial.println("Posicion Y = "+String(SyENCS));
               
              //controlAngulo();
              distancia = sensor.readRangeContinuousMillimeters();
             
              Sdistancia = String(convert_float_to_str(distancia/1000));
                
              Sangs = String(convert_int_to_str(angs));
               
              Sangz = String(convert_float_to_str(angz));//
              
              SxMPU = String(convert_float_to_str(xMPU*4.125*11.45));//

              SyMPU = String(convert_float_to_str(yMPU*4.125*36));//

              SxENCS = String(convert_float_to_str(xENCS*0.00120155*0.10042));

              SyENCS = String(convert_float_to_str(yENCS*0.000022493));//*

              StethaENCS = String(convert_float_to_str(tethaENCS*180/(M_PI*10000)));
              
                
              String Sdata = Sdistancia +";"+ Sangs +";"+ Sangz +";"+ StethaENCS +";"+ SxMPU + ";"+ SyMPU+ ";"+ SxENCS+ ";"+ SyENCS;
     
              //envia los datos a bluetooth
              SerialBT.println(Sdata);
                   
              numeros_T = 0;   //Se reinicia contador de numeros enteros de tiempo de muestreo
      }
           

      //recibe datos en caso de que sean transmitidos
      if(SerialBT.available()){
          valor=SerialBT.readString();  
          refe_MD = s.separa(valor,';',0).toInt();    //Recibe velocidad deseada desde MATLAB motor derecho
          refe_MI = s.separa(valor,';',1).toInt();    //Recibe velocidad deseada desde MATLAB motor izquierdo

      }
   
  }


//------------------------------------------------------------------------------movimiento del carro----------------------------------------------------------------------------------//
void movimiento(int PWM_MD,int PWM_MI){
      // Se elige polaridad de los motores si se necesita alcanzar una velocidad angular negativa
      if(Wp_MD >= 0){
          digitalWrite(PinMD1,LOW);
          digitalWrite(PinMD2,HIGH);
      }else{
          digitalWrite(PinMD1,HIGH);
          digitalWrite(PinMD2,LOW);
       }
      if(Wp_MI >= 0){
          digitalWrite(PinMI1,LOW);
          digitalWrite(PinMI2,HIGH);
      }else{
          digitalWrite(PinMI1,HIGH);
          digitalWrite(PinMI2,LOW);
       }
      //Se aplica PWM a cada motor
      ledcWrite(channelMI,PWM_MI);   
      ledcWrite(channelMD,PWM_MD);
}

//-----------------------------------------------------------------------------toma de datos de la MPU--------------------------------------------------------------------------------//
void MPU6050(){
  
       //Leer los valores del Acelerometro de la IMU
       Wire.beginTransmission(MPU);
       Wire.write(0x3B); //Pedir el registro 0x3B - corresponde al AcX
       Wire.endTransmission(false);
       Wire.requestFrom(MPU,6,true);   //A partir del 0x3B, se piden 6 registros
       AcX=Wire.read()<<8|Wire.read(); //Cada valor ocupa 2 registros
       AcY=Wire.read()<<8|Wire.read();
       AcZ=Wire.read()<<8|Wire.read();

       Acc[0] = 9.81*AcX/A_R;
       if(Acc[0]< 1){Acc[0] = 0;} // Se iguala a cero si el carro esta quieto
       //A partir de los valores del acelerometro, se calculan los angulos Y, X
    
     
       //Leer los valores del Giroscopio
       Wire.beginTransmission(MPU);
       Wire.write(0x43);
       Wire.endTransmission(false);
       Wire.requestFrom(MPU,6,true);   //A partir del 0x43, se piden 6 registros
       GyX=Wire.read()<<8|Wire.read(); //Cada valor ocupa 2 registros
       GyY=Wire.read()<<8|Wire.read();
       GyZ=Wire.read()<<8|Wire.read();
     
       
       //Calculo del angulo del Giroscopio
       Gy[2] = GyZ/G_R;//
       if(abs(Gy[2])< 8){Gy[2] = 0;} 
       dt = (millis() - tiempo_prev) / 1000.0;
       tiempo_prev = millis();
       
       //Integración respecto del tiempo paras calcular el YAW 
       Angle[2] = Angle[2]+Gy[2]*dt ;

       //integración para calcular la velocidad vp (velocidad en x)
       vp = Acc[0]*dt; 
       wp = Gy[2];
       angz= Angle[2];

       //calculo de la pocisión de la MPU en el MR inercial
       dxMPU = vp* cos((M_PI/180)*angz);
       dyMPU = vp* sin((M_PI/180)*angz);
       xMPU = xMPU + (dt*dxMPU);
       yMPU = yMPU + (dt*dyMPU);
       
}

//-----------------------------------------------------------------------toma de datos del encoder Motores--------------------------------------------------------------------------------//
void velocidad_M(){
      contador_MD = 0;
      contador_MI = 0;

      tiempo_total = 0;
      tiempo_actual = millis();
      while (tiempo_total < 24){   // Establece un tiempo de muestra de 25 ms para calcular velocidad actual de los motores
        attachInterrupt(PinA_MD, conteo_M, CHANGE);
        attachInterrupt(PinB_MD, conteo_M, CHANGE);
        attachInterrupt(PinA_MI, conteo_M, CHANGE);
        attachInterrupt(PinB_MI, conteo_M, CHANGE);
        tiempo_total = millis() - tiempo_actual;    
      }
      //Se calculan velocidades angulares
      Wp_MD = 104.7197*contador_MD/tiempo_total;  //Conversion (60000/600)* 2pi/60   
      Wp_MI = 104.7197*contador_MI/tiempo_total;  //Conversion (60000/600)* 2pi/60  
      
      // Se calcula velocidades del carro
      vpENCS = (radio/2) * (Wp_MD + Wp_MI);
      wpENCS = (radio/longitud) * (Wp_MD - Wp_MI);
      
      //Se calcula angulo obtenido desde encoders 
      tethaENCS =  tethaENCS + (wpENCS*tiempo_total);

      //Se calculan posicion desde un marco de referencia inercial con ENCODERS
      dxENCS = vpENCS * cos(tethaENCS/10000);//tethaENCS
      dyENCS = vpENCS * sin(tethaENCS/10000);
      xENCS = (xENCS + (tiempo_total*dxENCS));//*0.00120155
      yENCS = (yENCS + (tiempo_total*dyENCS));//*0.027413587

      numeros_T++;   // Se aumenta el numero de periodos 
  }

//----------------------------------------------------------------------controloes velocidad---------------------------------------------------
void controlPID_MD(float ref_MD){
      e_MD = ref_MD - Wp_MD;
      //u_MD = b2_MD*emd_MD +b1_MD*emu_MD + b0_MD*e_MD + umu_MD;  
      u_MD = 0.436614*emd_MD - 1.379132*emu_MD + 0.98397*e_MD + umu_MD;
      emd_MD = emu_MD;
      emu_MD = e_MD;
      umu_MD = u_MD;
      //Se limita el PWM de salida a maximo 200 y minimo 0
      if(u_MD < 0){u_MD = 0;} 
      if(u_MD > 200){u_MD = 200;}
      PWM_MD = u_MD;
}

void controlPID_MI(float ref_MI){
      e_MI = ref_MI - Wp_MI;
      //u_MI = b2_MI*emd_MI +b1_MI*emu_MI + b0_MI*e_MI + umu_MI;
      u_MI = 0.589252*emd_MI - 1.799428*emu_MI + 1.2632*e_MI + umu_MI; 
      emd_MI = emu_MI;
      emu_MI = e_MI;
      umu_MI = u_MI;
      //Se limita el PWM de salida a maximo 200 y minimo 0
      if(u_MI < 0){u_MI = 0;}
      if(u_MI > 200){u_MI = 200;}
      PWM_MI = u_MI;
}

void controlAngulo(){
      Xactual = xENCS;
      Yactual = yENCS;
      TETHAactual = tethaENCS/10000; //Conversion de theta a radianes
      
      TETHAdeseado=atan((Ydeseado-Yactual)/(Xdeseado-Xactual));
      TETHAe=(TETHAdeseado-TETHAactual);
    
      b0 = (Kp*PM + Ki*pow(PM,2)+ Kd)/(PM);
      b1 = -(Kp*PM + 2*Kd)/(PM);
      b2 = Kd/PM;

      VPdeseado = 2;
      WPdeseado = WPdeseado + b0*TETHAe + b1*TETHAe_1 +b2*TETHAe_2;


      TETHAe_2=TETHAe_1;
      TETHAe_1=TETHAe;

      WDdeseado=VPdeseado/radio +  longitud*WPdeseado/(2*radio);
      //Se limita velocidad angular a maximo 500 rad/s y minimo -500rad/s
      if (WDdeseado > 500){WDdeseado=500;}
      if (WDdeseado  < -500){WDdeseado=-500;} 
     
      WIdeseado=VPdeseado/longitud - longitud*WPdeseado/(2*radio);
      //Se limita velocidad angular a maximo 500 rad/s y minimo -500rad/s
      if (WIdeseado > 500){WIdeseado=500;}
      if (WIdeseado < -500){WIdeseado=-500;} 

}
//-------------------------------------------------------Funcion para evitar obstaculos--------------------------------------------------------------
void evitar(float as){
  
      //servo.write(as);
      if(as== 0){
        obs1x=cos((M_PI/180)*0)*(distancia/1000);
        obs1y=sin((M_PI/180)*0)*(distancia/1000);
        if(obs1x>0.2){obs1=0;}
        if(obs1y>0.2){obs1=0;}
      }
      if(as== 45){
        obs2x=cos((M_PI/180)*45)*(distancia/1000);
        obs2y=sin((M_PI/180)*45)*(distancia/1000);
        if(obs2x>0.2){obs1=0;}
        if(obs2y>0.2){obs1=0;}
      }
      if(as== 90){
        obs3x=cos((M_PI/180)*90)*(distancia/1000);
        obs3y=sin((M_PI/180)*90)*(distancia/1000);
        if(obs3x>0.2){obs1=0;}
        if(obs3y>0.2){obs1=0;}
      }
      if(as== 135){
        obs4x=cos((M_PI/180)*135)*(distancia/1000);
        obs4y=sin((M_PI/180)*135)*(distancia/1000);
        if(obs4x>0.2){obs1=0;}
        if(obs4y>0.2){obs1=0;}
      }
      if(as== 180){
        obs5x=cos((M_PI/180)*180)*(distancia/1000);
        obs5y=sin((M_PI/180)*180)*(distancia/1000);
        if(obs5x>0.2){obs1=0;}
        if(obs5y>0.2){obs1=0;}
      }
    
      xto=cos(tethaENCS/10000)*(ks1*obs1x+ks2*obs2x+ks3*obs3x+ks4*obs4x+ks5*obs5x);
      yto=sin(tethaENCS/10000)*(ks1*obs1y+ks2*obs2y+ks3*obs3y+ks4*obs4y+ks5*obs5y);
    
      THETAto=atan2(yto,xto);
}

//------------------------------------------------------------------------tarea para pasar de float a string--------------------------------------------------------------------------//
char* convert_float_to_str(float i) { // converts int16 to string. Moreover, resulting strings will have the same length in the debug monitor.
      sprintf(tmp_str, "%f", i);
      return tmp_str;
}

char* convert_int_to_str(int i) { // converts int16 to string. Moreover, resulting strings will have the same length in the debug monitor.
      sprintf(tmp_str_int, "%d", i);
      return tmp_str_int;
}
